# AdoteFGA

Encontre um animalzinho perfeito pra você.

# Sobre 

Antes de tudo precisamos entender a problematica de adoção de animais na fga.

# Que problematica é essa?

Na FGA existe uma grante quantidade de gatos e cachorros sem dono, vagando a procura de comida.Os meios de comunicação dos alunos para doar comida ou adotar é o whatsApp e o facebook.
No whatsApp existe um grupo em que as garotas da FGA usam parar informar uma gama de informações relacionado a faculdade.
No Facebook existe o grupo da FGA onde os todos os alunos usam para infomar noticias relacionados a FGA.
O problema desses meios de comunicação é que as noticias relacionada a os animais ficam perdidas pela grande quantidade de noticias postadas.
Pensando nisso esse blog veio para fazer com que os alunos possam postar noticias apenas de animais que precisam ser adotados.

# Como funciona 

Os alunos que querem olhar as postagens podem apenas entrar no site, que serão redirecionados para pagina de posts. 

*  Para  ter acesso a pagina de Novo Post ou Novo Evento e criar um post ou um novo Evento,  precisa-se fazer um cadastro, ou se ja estiver cadastrado,fazer o login da pagina. 

*  Para fazer algum comentario na postagem também precisam fazer o login na pagina.

*  Para apagar a postagem ou comentario somente o usuario administrador poderá ter esse acesso.

# Para ativar o server

* Entre na Pasta adotefga
* No terminal:
``
rails server
``
* Digite no seu navegador:

[localhost:3000](url)


# Para melhor visualização da aplicação foram feitas os diagramas das models e controllers e o caso de uso

![Caso de uso](https://gitlab.com/daniela0412/ep3_oo/blob/devc/adotefga/docs/caso_de_uso1.png)
![Controllers](https://gitlab.com/daniela0412/ep3_oo/blob/devc/adotefga/docs/controllers1.png)
![models](https://gitlab.com/daniela0412/ep3_oo/blob/devc/adotefga/docs/models1.png)









